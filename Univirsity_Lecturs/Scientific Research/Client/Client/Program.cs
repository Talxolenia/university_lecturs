﻿using Client.Code;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creating Socket Object
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //File Oppening
            FileStream file = null;
            try
            {
                Console.WriteLine("Opening File " + ConfigurationManager.AppSettings["fileToSend"] + " .........");
                file = File.Open(ConfigurationManager.AppSettings["fileToSend"], FileMode.Open);
                Console.WriteLine();
            }
            catch
            {
                Console.WriteLine("Error Reading File");
                Environment.Exit(-1);
            }


            Console.WriteLine("Press any key to connect to the server...");
            Console.ReadKey();
            Console.WriteLine("Connecting to the host... " + ConfigurationManager.AppSettings["IpAddressToSend"] + ":" + ConfigurationManager.AppSettings["portToSend"]);
            //Establishing connection with the server
            byte[] ipAddress = ConfigurationManager.AppSettings["IpAddressToSend"].Split(new char[] { '.' }).Select(item => Convert.ToByte(item)).ToArray();
            try
            {
                socket.Connect(new IPAddress(ipAddress), Convert.ToInt32(ConfigurationManager.AppSettings["portToSend"]));
            }
            catch
            {
                Console.WriteLine("Unable to connect...");
            }


            if (socket.Connected)
            {
                Console.WriteLine("Conection Established!, connected to " + ConfigurationManager.AppSettings["IpAddressToSend"] + ", port: " + ConfigurationManager.AppSettings["portToSend"]);
                Console.WriteLine();
                byte[] command = new byte[1];

                try
                {

                    //sending file name length
                    int fileNameLenght = ConfigurationManager.AppSettings["fileToSend"].Length;
                    socket.Send(fileNameLenght.ToByteArray(), 4, SocketFlags.None);

                    //sending file name
                    byte[] fileNameArr = ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["fileToSend"]);
                    socket.Send(fileNameArr, fileNameLenght, SocketFlags.None);

                    //sendign file length
                    socket.Send(file.Length.ToByteArray(), 8, SocketFlags.None);

                    socket.Receive(command, 1, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(0);
                }

                if (ASCIIEncoding.ASCII.GetString(command).ToLower() == "y")
                {

                    byte[] buffer = new byte[1024];
                    byte[] fileContent = new byte[file.Length];
                    file.Read(fileContent, 0, (int)file.Length);

                    for (long i = 0; i < file.Length; i += 1024)
                    {
                        try
                        {
                            if (i + 1024 > file.Length)
                            {
                                Array.Copy(fileContent, i, buffer, 0, Math.Abs(file.Length - i));
                                socket.Send(buffer, (int)Math.Abs(file.Length - i), SocketFlags.None);
                                break;
                            }

                            Array.Copy(fileContent, i, buffer, 0, 1024);
                            socket.Send(buffer, 1024, SocketFlags.None);
                        }
                        catch (SocketException ex)
                        {
                            Console.WriteLine(ex.Message);
                            Environment.Exit(0);
                        }
                    }

                    Console.WriteLine("Send Complete..");
                }
            }
            else
            {
                Console.WriteLine("Unable to connect...");
            }

            socket.Dispose();
            file.Close();
            file.Dispose();

            Console.WriteLine("Press any key to end");
            Console.ReadKey();
        }
    }
}

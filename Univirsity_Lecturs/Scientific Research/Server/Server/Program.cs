﻿using Server.Code;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = string.Empty;
            byte[] intByteBuffer = new byte[4];
            byte[] longByteBuffer = new byte[8];
            byte[] fileContent = null;
            int size = 0;
            long fileSize = 0;

            //creating socket
            Console.WriteLine("Initelizing Server...");
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Console.WriteLine("Initeliziation complete");

            //binding a socket to port
            EndPoint endPoint = new IPEndPoint(IPAddress.Parse(ConfigurationManager.AppSettings["IpAddressToListen"]), Convert.ToInt32(ConfigurationManager.AppSettings["portToListen"]));
            listener.Bind(endPoint);

            //listening to a port
            listener.Listen(5);
            Console.WriteLine("Listening to port " + ConfigurationManager.AppSettings["portToListen"]);

            Socket clientSocket = listener.Accept();

            try
            {
                //Reciving file name size
                clientSocket.Receive(intByteBuffer, 4, SocketFlags.None);
                size = intByteBuffer.ToInt32();

                //Reciving file name
                byte[] fileNameArr = new byte[size];
                clientSocket.Receive(fileNameArr, size, SocketFlags.None);
                fileName = ASCIIEncoding.ASCII.GetString(fileNameArr);

                //Reciving file size
                clientSocket.Receive(longByteBuffer, 8, SocketFlags.None);
                fileSize = longByteBuffer.ToInt64();
            }
            catch(SocketException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(0);
            }

            Console.WriteLine("Do you want to recive file {0}, size {1} KB ? (y/n)", fileName, fileSize / 1024);
            string command = Console.ReadLine().ToLower();

            if (command == "no")
            {
                try
                {
                    clientSocket.Send(ASCIIEncoding.ASCII.GetBytes("n"), 1, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(0);
                }
            }
            else
            {
                try
                {
                    clientSocket.Send(ASCIIEncoding.ASCII.GetBytes("y"), 1, SocketFlags.None);
                    Console.WriteLine("Reciving file... ");

                    byte[] buffer = new byte[1024];
                    fileContent = new byte[fileSize];
                    double updateRate = (fileSize / 1024) + 0.1;
                    double rate = 0;

                    for (long i = 0; i < fileSize; i += 1024)
                    {

                        size = clientSocket.Receive(buffer, 1024, SocketFlags.None);

                        lock(fileContent)
                        {
                            Array.Copy(buffer, 0, fileContent, i, size);
                        }

                        Console.Write("\rReciving {0}%", (byte)(rate++ * 100 / updateRate));

                    }
                }
                catch (SocketException ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(0);
                }

                Console.WriteLine();
                Console.WriteLine("Reciving 100% completed...!");

                //Saving file
                FileStream recivedFile = File.OpenWrite(fileName);
                recivedFile.Write(fileContent, 0, fileContent.Length);

                recivedFile.Close();
                recivedFile.Dispose();

                Console.WriteLine("File" + fileName + " Recived Successfully");
            }

            if (clientSocket.Connected)
            {
                clientSocket.Disconnect(false);
       
            }

            if(listener.Connected)
            {
                listener.Disconnect(false);
            }

            clientSocket.Dispose();
            listener.Dispose();

            Console.WriteLine("Press any key to end.");
            Console.ReadKey();
        }
    }
}
